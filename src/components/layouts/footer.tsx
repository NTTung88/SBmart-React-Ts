function Footer() {

    return (
        <footer className="bg-white">
            <div className="text-center mb-5">Power by White Label</div>
        </footer>
    );
}

export default Footer;