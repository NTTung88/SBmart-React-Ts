import { Outlet } from "react-router-dom";
import Footer from "./footer";
import Header from "./header";

const Layout = (props: { children: any }) => {
    return (
        <>
            <Header />
            {props.children}
            <Outlet />
            <Footer />
        </>
    );
};

export default Layout;