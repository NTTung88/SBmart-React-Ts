import React from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './app/store';
import Layout from './components/layouts';
import Routers from './routers/routers';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Provider store={store}>
          <Layout>
            <Routers />
          </Layout>
        </Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
