import React, { useContext } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../pages";
import { endpoints } from "./endpoints";


export default function Routers() {

    return (
        <Routes>
            <Route path={endpoints.home} element={<Home />} />


        </Routes>
    );
}

export const MenuRouters = [

];
